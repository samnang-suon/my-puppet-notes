# PCP_01_03h07m_PLURALSIGHT_Getting Started with Puppet
Q: Why do we need a tool like Puppet ?  
A: Clicking through multiple GUI screens to setup virtual computers can be tedious.
First, there were Terraform and Vagrant:

    Vagrant and Terraform are both projects from HashiCorp.
    Vagrant is a tool focused for managing development environments and Terraform is a tool for building infrastructure.

https://www.vagrantup.com/intro/vs/terraform

## Infrastructure As Code (IAC)
![InfrastructureAsCode01](images/InfrastructureAsCode01.png)

![InfrastructureAsCode02](images/InfrastructureAsCode02.png)

Q: What make Puppet different from other tools ?  
A: Puppet use a declarative language (Puppet DSL) instead of a step-by-step (imperative way).

Q: Describe the Puppet architecture ?  
A:

![PuppetArchitecture01](images/PuppetArchitecture01.png)

Q: What is the agent run lifecycle ?  
A:

![TheAgentRunLifecycle](images/TheAgentRunLifecycle.png)

Q: Nano Text Editor ?  
A: Launch nano using 'nano -u' to enable the undo feature. For more keyboard shortcuts:

![NanoCommandQuickGuide](images/NanoCommandQuickGuide.png)

Q: Give an example of 'notify' resource ?  
A: Save the following code as 'greeting.pp':
```shell
notify {'greeting':
  message => 'Welcome to pluralsight!',
}
notify {'course':
  message => 'Getting started with Puppet',
}
```
Q: How to lint your puppet code ?  
A: Using the following command:

    puppet parser validate path/to/puppet/manifest/file

Q: How to you run a puppet manifest ?  
A: Using the following command:

    puppet apply /path/tp/puppet/manifest/file

Q: How do you write a module ?  
A: Create a 'init.pp' file inside 'cowsay/manifests' directory
```shell
class cowsay {
  package {'gem':
    ensure   => present,
    provider => 'yum',
  }
  package {'cowsay':
    ensure   => present,
    provider => 'gem',
  }
}
```
# ==================================================
# 02h45m_LYNDA_Puppet Essential Training 2019-2020 (INT)

## Resources
![PuppetResources](images/PuppetResources.png)

## Structure Of Resources
![StructureOfAResource](images/StructureOfAResource.png)

## Class Definition
![ClassDefinitionSyntax](images/ClassDefinitionSyntax.png)

## Param Class
![ParamClass](images/ParamClass.png)

## DAG
![LookingAtTheDAG](images/LookingAtTheDAG.png)

## Relationship Metaparameter
![RelationshipMetaparameter](images/RelationshipMetaparameter.png)

## Variables
![PuppetVariableDoNotVary](images/PuppetVariableDoNotVary.png)

Variables are immutables.

### Puppet Data Types
![PuppetString](images/PuppetString.png)

![OtherDataTypes](images/OtherDataTypes.png)

![PuppetArray](images/PuppetArray.png)

### If_Else
![PuppetIfElse](images/PuppetIfElse.png)

![PuppetSwitchCase](images/PuppetSwitchCase.png)

![PuppetSelector](images/PuppetSelector.png)

### Loop
![PuppetEach](images/PuppetEach.png)

## Fact/Facter
![PuppetFact](images/PuppetFact.png)

## Function
![PuppetFunction](images/PuppetFunction.png)

## Template Function
![PuppetTemplateFunction](images/PuppetTemplateFunction.png)

## Lookup Function
![PuppetLookup](images/PuppetLookup.png)

## File Source
![PuppetFileSource](images/PuppetFileSource.png)

## Resource Default
![PuppetResourceDefault](images/PuppetResourceDefault.png)

# ==================================================
# 01h54m_LYNDA_Learning Puppet 2019
Q: What is Puppet ?  
A: "In computing, Puppet is a software configuration management tool which includes its own declarative language to describe system configuration." -wikipedia

![PuppetAbstraction](images/PuppetAbstraction.png)

![PuppetAbstractionModule](images/PuppetAbstractionModule.png)

![PuppetAbstractionProfileAndRole](images/PuppetAbstractionProfileAndRole.png)

![PuppetReporting](images/PuppetReporting.png)

## Puppet Jargon
* Node: An individual server or device managed by Puppet.
* Resources: Single units of configuration in Puppet.
* Class: A collection of Puppet code that make sense as a logical group.
* Manifest: A text file for holding Puppet code. (.pp)
* Profile: A class that defines a specific set of configuration.
* Role: A class that defines the business role of a node.

References: https://puppet.com/docs/puppet/7.5/platform.html

## Set up our working lab
1. Need to install Puppet
2. Need to install a Puppet Master

For more info: https://puppet.com/docs/puppet/7.5/installing_and_upgrading.html

## File Resource
![ExampleOfFileResource](images/ExampleOfFileResource.png)

## Package Resource
![ExampleOfPackageResource](images/ExampleOfPackageResource.png)

## Built-in Resources
![BuiltInResourceTypes](images/BuiltInResourceTypes.png)

## Site.pp Demo
```puppet
node default {
    file { '/root/README.md':
        ensure  => file,
        content => '# Welcome to README.md',
    }
}
# To run this script, type:
puppet apply your_puppet_script.pp
# source:
# https://ask.puppet.com/question/26472/how-to-runexecute-puppet-script-manually/
```
![SitePPDemo](images/SitePPDemo.png)

## Class definition
![ExampleClassDefinition](images/ExampleClassDefinition.png)

## Puppet Forge
![PuppetForge](images/PuppetForge.png)

See: https://forge.puppet.com/

## R10K and Puppetfile
see: 
* https://github.com/puppetlabs/r10k
* https://puppet.com/docs/pe/2019.8/r10k.html

## Role vs Profile
see: https://puppet.com/docs/pe/2019.8/osp/the_roles_and_profiles_method.html#configuring_roles_and_profiles

## Orchestration
![PuppetOrchestration](images/PuppetOrchestration.png)

![PuppetBolt](images/PuppetBolt.png)

![WhyUsePuppetBolt](images/WhyUsePuppetBolt.png)

## Puppet Module
![DirectoryManifest](images/DirectoryManifest.png)

![DirectoryFiles](images/DirectoryFiles.png)

![DirectoryTemplates](images/DirectoryTemplates.png)

![DirectoryLib](images/DirectoryLib.png)

![DirectoryTask](images/DirectoryTask.png)

![DirectoryOther](images/DirectoryOther.png)

![DirectoryMetadata](images/DirectoryMetadata.png)

![DirectoryReadme](images/DirectoryReadme.png)


# ==================================================
# 02h55m_PACKT_Puppet 6 Essentials
![ConfigurationManagementSystem](images/ConfigurationManagementSystem.png)

Q: What do you need to do before installing the Puppet Server/Agent ?  
A: You need to add the puppetlabs repo to the package management:

    wget https://apt.puppet.com/puppet7-release-focal.deb
    sudo dpkg -i puppet7-release-focal.deb

References:
* https://puppet.com/docs/puppet/5.5/puppet_platform.html
* https://www.howtoforge.com/tutorial/ubuntu_puppet/

Q: What happen when you install the Puppet Server ?  
A: You also install the Puppet Agent:

    apt-get install puppetserver

Reference: https://puppet.com/docs/puppetserver/5.3/install_from_packages.html

    sudo apt-get install puppet-agent

Reference: https://puppet.com/docs/puppet/5.5/install_linux.html

Q: What is TimeSync ?  
A: The Puppet server and agent should use the same timezone.

![TimeSync](images/TimeSync.png)

For that, we run the following command:

    puppet apply -e 'package { "chrony": ensure => installed }'
    puppet apply -e 'service { "chronyd": ensure => running, enable => true }'

OR

    sudo /opt/puppetlabs/bin/puppet apply -e 'package {"chrony": ensure => installed }'
    sudo /opt/puppetlabs/bin/puppet apply -e 'service {"chronyd": ensure => running, enable => true }'

![UbuntuChronyInstallation](images/UbuntuChronyInstallation.png)

Q: Example of Puppet power ?  
A:
```shell
# source:
# https://forge.puppet.com/modules/puppetlabs/apache/dependencies
sudo /opt/puppetlabs/bin/puppet module install puppetlabs-apache --version 6.0.0
sudo /opt/puppetlabs/bin/puppet apply -e 'include apache'
curl localhost
```
![ExampleOfPuppetPower](images/ExampleOfPuppetPower.png)

## Manifest File
![WhatIsAManifestFile](images/WhatIsAManifestFile.png)

## Puppet Config Command
![PuppetConfigCommand](images/PuppetConfigCommand.png)

Q: How to I see a list of Puppet existing configurations ?  
A: Use the following command:

    sudo /opt/puppetlabs/bin/puppet config print

Q: Where Puppet stored its config file ?  
A: Use the following command:

    sudo /opt/puppetlabs/bin/puppet config print config

![WherePuppetStoreItsConfigFile](images/WherePuppetStoreItsConfigFile.png)

Q: What is the default manifest directory/file that my Puppet is using ?  
A: Use the following command:

    sudo /opt/puppetlabs/bin/puppet config print manifest

![WhichManifestAmIUsing](images/WhichManifestAmIUsing.png)

## Puppet Production Directory
![PuppetProductionEnvironmentScript](images/PuppetProductionEnvironmentScript.png)

## Create Environment
![CreateEnvironment](images/CreateEnvironment.png)

## Puppet Resource
![PacktPuppetResource](images/PacktPuppetResource.png)

## List Puppet Resource Types
![PacktListPuppetResources](images/PacktListPuppetResources.png)

## Puppet Agent Default Environment
Q: How do you set the Puppet Agent to run manifests from a different directory than 'production' ?  
A: Use the following commands:

    puppet config set environment [EnvName] --section=agent

![HowToSetTheAgentDefaultEnvironment](images/HowToSetTheAgentDefaultEnvironment.png)

## Bash Alias Demo
![BashAliasExample](images/BashAliasExample.png)

![UbuntuBashrcFile](images/UbuntuBashrcFile.png)

Q: How do you get a list of all Puppet resources ?  
A: Use the following commands:

    sudo /opt/puppetlabs/bin/puppet resource --types

Reference: https://puppet.com/docs/puppet/5.5/man/resource.html

Q: How to get more info about a resource ?  
A: Use the following command:

    sudo /opt/puppetlabs/bin/puppet describe [PuppetResourceType]

    Examples:
    sudo /opt/puppetlabs/bin/puppet describe package
    sudo /opt/puppetlabs/bin/puppet describe service
    sudo /opt/puppetlabs/bin/puppet describe service chrony

    Examples:
    sudo /opt/puppetlabs/bin/puppet resource package
    sudo /opt/puppetlabs/bin/puppet resource service chrony

## Puppet Module
![PacktPuppetModule](images/PacktPuppetModule.png)

### The Big 3 Resource
![PacktTheBig3Resource](images/PacktTheBig3Resource.png)

![PacktTheModuleManifest](images/PacktTheModuleManifest.png)

![PacktTheServiceManifest](images/PacktTheServiceManifest.png)

## Puppet Conditionals
![PacktUsingConditionals](images/PacktUsingConditionals.png)

Q: Where are Puppet module stored after installation ?  
A: Use the following commands:
```shell
sudo /opt/puppetlabs/bin/puppet module list
# You will see:
# /etc/puppetlabs/code/environments/production/modules (no modules installed)
# /etc/puppetlabs/code/modules (no modules installed)
# /opt/puppetlabs/puppet/modules (no modules installed)

sudo /opt/puppetlabs/bin/puppet module install puppetlabs-apache
# You will see:
# /etc/puppetlabs/code/environments/production/modules
# └─┬ puppetlabs-apache (v6.0.0)
# ├── puppetlabs-concat (v7.0.1)
# └── puppetlabs-stdlib (v7.0.1)
```

By default, all modules will be installed inside '/etc/puppetlabs/code/environments/production/modules'

Q: How to install a module to another directory ?  
A: Use the following commands:
```shell
sudo /opt/puppetlabs/bin/puppet module install [ModuleName] -i destination/folder
Examples:
sudo /opt/puppetlabs/bin/puppet module install puppetlabs-apache -i /etc/puppetlabs/code/modules
```

# ==================================================
# Useful Websites
* https://puppet.com/docs/puppet/7.5/puppet_index.html (Welcome to Puppet 7.5.0 Documentation)

# ================================================== BEGIN TUTORIAL
# Install Puppet using 2 VMs
**NOTE** I did try to have 1 acts as both a master and an agent node but I wasn't to make it work...

The following steps/tutorial is based on: https://phoenixnap.com/kb/install-puppet-ubuntu
## Set up master node
ifconfig

![MasterNodeIfconfig](images/MasterNodeIfconfig.png)

## Set up agent node
ifconfig

![AgentNodeIfconfig](images/AgentNodeIfconfig.png)

## Hostname resolution
on the master node:
```shell
sudo nano /etc/hosts

# MY PUPPET CONFIG ON MASTER NODE
192.168.72.136 puppetmaster puppet
192.168.72.135 puppetclient
```
![SetHostnameOnMasterNode](images/SetHostnameOnMasterNode.png)

**NOTE** The master node need to have 'puppet' alias. Otherwise, you will get:
![PuppetPortError](images/PuppetPortError.png)

On the agent node:
```shell
sudo nano /etc/hosts

# MY PUPPET CONFIG ON AGENT NODE
192.168.72.136 puppetmaster puppet
192.168.72.135 puppetclient
```
![SetHostnameOnAgentNode](images/SetHostnameOnAgentNode.png)

## Install Puppet Package
### On the master node:
```shell
# Add puppetlabs to apt index
wget https://apt.puppetlabs.com/puppet6-release-focal.deb
sudo dpkg -i puppet6-release-focal.deb

# Install puppetserver package
sudo apt-get update --yes
sudo apt-get install puppetserver --yes

# Check puppetserver config
cat /etc/puppetlabs/puppet/puppet.conf

# Update puppetserver java memory usage
sudo nano /etc/default/puppetserver

# Check and start Puppet server
sudo systemctl restart puppetserver && \
sudo systemctl enable puppetserver && \
sudo systemctl status puppetserver
```
![MasterNodePuppetConf](images/MasterNodePuppetConf.png)

### On the agent node:
```shell
# Add puppetlabs to apt index
wget https://apt.puppetlabs.com/puppet6-release-focal.deb
sudo dpkg -i puppet6-release-focal.deb

# Install puppet-agent package
sudo apt-get update --yes
sudo apt-get install puppet-agent --yes

# Update config file
sudo nano /etc/puppetlabs/puppet/puppet.conf
# [main]
# certname = puppetclient
# server = puppetmaster
```
![AgentNodePuppetConf](images/AgentNodePuppetConf.png)
```shell
# Check and start Puppet server
sudo systemctl restart puppet && \
sudo systemctl enable puppet && \
sudo systemctl status puppet
```
## Test network between master and agent
use ping utility:
### Master to Agent node
![MasterNodePingAgentNode](images/MasterNodePingAgentNode.png)

![PingAgentNodeFromMasterNode](images/PingAgentNodeFromMasterNode.png)
### Agent to Master node
![AgentNodePingMasterNode](images/AgentNodePingMasterNode.png)

![PingMasterNodeFromAgentNode](images/PingMasterNodeFromAgentNode.png)

## Configure communication between master and agent
## Send certificate
On the master node:
```shell
sudo /opt/puppetlabs/bin/puppetserver ca list --all
```
![MasterNodeListPendingCertificate](images/MasterNodeListPendingCertificate.png)

On the agent node:
```shell
sudo /opt/puppetlabs/bin/puppet agent --test
# OR
puppet agent --test
```
![AgentNodeRequestCertificate](images/AgentNodeRequestCertificate.png)

### Sign certificate
On the master node:
```shell
sudo /opt/puppetlabs/bin/puppetserver ca sign --all
```

## Puppet Command without alias (optional)
Use the right command:

    sudo /opt/puppetlabs/bin/puppetserver
    sudo /opt/puppetlabs/bin/puppet

![PuppetServerVsAgentVersion](images/PuppetServerVsAgentVersion.png)

## Puppet Command to $PATH (optional)

    echo $PATH
    export PATH="$PATH:/opt/puppetlabs/bin"

![AddPuppetToPathVariable](images/AddPuppetToPathVariable.png)

Reference: https://linuxize.com/post/how-to-add-directory-to-path-in-linux/

### To remove from $PATH

    echo $PATH
    export PATH=/all/path/without/the/one/you/want/to/remove

Reference: https://stackoverflow.com/questions/11650840/remove-redundant-paths-from-path-variable
# ================================================== END TUTORIAL

# ================================================== BEGIN TUTORIAL 02
# Sources:
* https://www.howtoforge.com/tutorial/ubuntu_puppet/
* https://phoenixnap.com/kb/install-puppet-ubuntu
* https://tecadmin.net/how-to-install-puppet-on-ubuntu-20-04/
* https://www.osradar.com/how-to-install-puppet-on-ubuntu-20-04/
* https://idroot.us/install-puppet-ubuntu-20-04/
* https://linuxtips.us/install-puppet-ubuntu-20-04/
## Update apt index for BOTH (Master and Agent) nodes
```shell
sudo apt update && sudo apt upgrade
```

## Update hostnames
On the master node:
```shell
ifconfig
```

On the agent node:
```shell
ifconfig
```

On the master node:
```shell
sudo nano /etc/hosts

# MY PUPPET CONFIG ON THE MASTER NODE
192.168.72.136 puppetmaster puppet
192.168.72.137 puppetclient
```

On the agent node:
```shell
sudo nano /etc/hosts

# MY PUPPET CONFIG ON THE AGENT NODE
192.168.72.136 puppetmaster puppet
192.168.72.137 puppetclient
```

## Install Puppetlabs repo
On the master node:
```shell
cd /tmp
wget https://apt.puppetlabs.com/puppet7-release-focal.deb
sudo apt install ./puppet7-release-focal.deb
sudo apt update
sudo apt install puppetserver --yes

# Update JVM for Puppet
sudo nano /etc/default/puppetserver

# Check and start puppetserver
sudo systemctl stop puppetserver
sudo systemctl start puppetserver
sudo systemctl enable puppetserver
```

On the agent node:
```shell
cd /tmp
wget https://apt.puppetlabs.com/puppet7-release-focal.deb
sudo apt install ./puppet7-release-focal.deb
sudo apt update
sudo apt install puppet-agent --yes

# Update puppet config file
sudo nano /etc/puppetlabs/puppet/puppet.conf
[main]
certname = puppetclient
server = puppet
# server = puppetmaster <--- Even though this is the value used on websites, I've noticed that 'puppet' value works better.

# Check and update puppet-agent
sudo systemctl stop puppet
sudo systemctl start puppet
sudo systemctl enable puppet
```
![PuppetConfThatWork](images/PuppetConfThatWork.png)
## Open 8140 port
```shell
sudo ufw allow 8140/tcp
```

## Sign certificate (SSL)
On the master node:
```shell
sudo /opt/puppetlabs/bin/puppetserver ca list --all
```

On the agent node:
```shell
sudo /opt/puppetlabs/bin/puppet agent --test
```

Back On the master node:
```shell
sudo /opt/puppetlabs/bin/puppetserver ca sign --all
```
# ================================================== END TUTORIAL 02
# Puppet IDE
![VSCodePuppetPlugin](images/VSCodePuppetPlugin.png)

# Puppet Folder Structure
See: 
* https://puppet.com/blog/magic-directories-guide-to-puppet-directory-structure/ (Magic directories: a guide to Puppet directory structure)
* https://puppet.com/docs/puppet/5.5/dirs_important_directories.html (Important directories and files)
* https://github.com/puppetlabs/control-repo (A Puppet Control Repository)

# Puppetfile
See:
* https://puppet.com/docs/pe/2019.8/puppetfile.html (Managing environment content with a Puppetfile)
* https://github.com/puppetlabs/r10k/blob/main/doc/puppetfile.mkd (Puppetfile r10k)

# Puppet Facter
See:
* https://puppet.com/docs/puppet/7.5/facter.html

# Puppet Development Kit (PDK)
See:
* https://puppet.com/docs/pdk/2.x/pdk.html (Welcome to Puppet Development Kit 2.1)

# Puppet Relationship and Ordering
See:
* https://puppet.com/docs/puppet/7.5/lang_relationships.html (Relationships and ordering)

# Puppet Class and its Parameters
See:
* https://puppet.com/docs/puppet/7.5/lang_classes.html#class-parameters-and-variables

# The Puppet Language
Official Doc: https://puppet.com/docs/puppet/7.5/puppet_language.html

# Resource Abstraction Layer (RAL)
See:
* https://puppet.com/docs/puppet/7.5/man/resource.html

Q: How to SSH into Ubuntu Linux ?  
A: see:
https://linuxize.com/post/how-to-enable-ssh-on-ubuntu-18-04/

Q: Tools comparisons ?  
A:

![Ansible_Vs_Puppet_Vs_Chef](images/Ansible_Vs_Puppet_Vs_Chef.png)

References: https://www.whizlabs.com/blog/chef-vs-puppet-vs-ansible/

Q: Puppet module management ?  
A: Use the following commands:

    sudo /opt/puppetlabs/bin/puppet module list
    sudo /opt/puppetlabs/bin/puppet module uninstall [ModuleName]

![PuppetModuleManagementExample](images/PuppetModuleManagementExample.png)

Q: When running 'puppet agent --test', which manifest will be run ?  
A: All manifest files inside:

    sudo /opt/puppetlabs/bin/puppet config print manifest
    OR
    /etc/puppetlabs/code/environments/production/manifests

![NotifyHelloExample](images/NotifyHelloExample.png)

![PuppetManifestDirectoryTree](images/PuppetManifestDirectoryTree.png)

# Puppet.conf
References:
* https://puppet.com/docs/puppet/7.6/config_file_main.html ---> puppet.conf: The main config file
* https://puppet.com/docs/puppet/7.6/configuration.html ---> puppet.conf Configuration Reference

# Puppet CLI
References:
* https://puppet.com/docs/puppet/7.6/man/index.html ---> Puppet CLI

# Extras
* puppet autosign
* puppet directory structure
* puppet namevar
* puppet resource title vs namevar
* puppet metaparameter
* puppet implied dependency
* puppet modern ruby api
* puppet dsl functions
* https://www.osboxes.org/
* https://puppet.com/docs/bolt/latest/module_structure.html
* https://puppet.com/docs/puppet/5.5/lang_template_epp.html
* https://puppet.com/docs/puppet/7.6/lang_template.html
* https://puppet.com/docs/puppet/5.5/lang_facts_and_builtin_vars.html#trusted-facts
* https://puppet.com/docs/puppet/7.6/lang_defined_types.html